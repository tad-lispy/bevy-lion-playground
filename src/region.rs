use bevy::prelude::*;
use std::ops::{Div, Mul};

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub struct Region {
    // Region value carries it's size to avoid confusing regions of different sizes
    // For regions to be comparable (Eq) and hashable size has to be integer based.
    size: u32,
    pub coordinates: IVec2,
}

impl Region {
    pub fn new(size: u32, coordinates: IVec2) -> Self {
        Self { size, coordinates }
    }

    fn from_vec3(size: u32, translation: &Vec3) -> Self {
        Self {
            coordinates: translation.truncate().div(size as f32).round().as_ivec2(),
            size,
        }
    }

    /// Return this region and it's neighbors (including diagonal) and their
    /// neighbors, and so on up to the given distance.
    pub fn with_naighbors(&self, distance: u8) -> Vec<Self> {
        let IVec2 { x, y } = self.coordinates;
        let span = -(distance as i32)..=(distance as i32);
        let mut regions = Vec::default();
        for delta_x in span.to_owned() {
            for delta_y in span.to_owned() {
                regions.push(Self {
                    size: self.size,
                    coordinates: IVec2::new(x + delta_x, y + delta_y),
                })
            }
        }
        return regions;
    }
}

pub trait InRegion {
    /// Given the size of a region, returns the region that the object belongs to.
    fn get_region(&self, size: u32) -> Region;
}

impl InRegion for Vec3 {
    fn get_region(&self, size: u32) -> Region {
        Region::from_vec3(size, self)
    }
}

impl From<Region> for Vec3 {
    /// A Vec3 pointing to the center of the region
    fn from(region: Region) -> Self {
        region
            .coordinates
            .mul(region.size as i32)
            .extend(0)
            .as_vec3()
    }
}

impl From<Region> for Vec2 {
    /// A Vec2 pointing to the center of the region
    fn from(region: Region) -> Self {
        region.coordinates.mul(region.size as i32).as_vec2()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ops::Not;

    #[test]
    fn comparison() {
        assert_eq!(
            Region::new(300, IVec2::new(4, -3)),
            Region::new(300, IVec2::new(4, -3)),
            "If they have same size and same coordinates, they are the same region."
        );

        assert_ne!(
            Region::new(300, IVec2::new(4, -3)),
            Region::new(200, IVec2::new(4, -3)),
            "If they have different sizes, they are not the same region."
        );
    }

    #[test]
    fn from_vec3() {
        assert_eq!(
            Vec3::ZERO.get_region(100),
            Region {
                size: 100,
                coordinates: IVec2::ZERO
            },
            "(0, 0, 0) should be right in the middle of region (0, 0)"
        );

        assert_eq!(
            Vec3::new(45.0, -45.0, 0.0).get_region(100),
            Region {
                size: 100,
                coordinates: IVec2::new(0, 0)
            },
            "Close to the edge of (0, 0)"
        );

        assert_eq!(
            Vec3::new(55.0, -55.0, 0.0).get_region(100),
            Region {
                size: 100,
                coordinates: IVec2::new(1, -1)
            },
            "Right off the edge of (0, 0)"
        );

        assert_eq!(
            Vec3::new(204.0, -158.0, 20.0).get_region(50),
            Region {
                size: 50,
                coordinates: IVec2::new(4, -3)
            },
            "Some random place in some small, random region"
        );

        assert_eq!(
            Vec3::new(204.0, -158.0, 20.0).get_region(200),
            Region {
                size: 200,
                coordinates: IVec2::new(1, -1)
            },
            "Same place in a large region"
        );
    }

    #[test]
    fn into_vec3() {
        assert_eq!(
            Vec3::from(Region::new(300, IVec2::ZERO)),
            Vec3::ZERO,
            "Center of region (0, 0) is at (0, 0, 0)"
        );

        assert_eq!(
            Vec3::from(Region::new(300, IVec2::new(1, 0))),
            Vec3::new(300.0, 0.0, 0.0),
            "The center of the region one to the south of zero"
        );

        assert_eq!(
            Vec3::from(Region::new(300, IVec2::new(-5, 3))),
            Vec3::new(-1500.0, 900.0, 0.0),
            "The center of some random region"
        )
    }

    #[test]
    fn into_vec3_and_back() {
        assert_eq!(
            Vec3::from(Region::new(300, IVec2::new(-5, 3))).get_region(300),
            Region::new(300, IVec2::new(-5, 3)),
            "The center of some random region"
        )
    }

    #[test]
    fn neighbors() {
        assert!(
            Region::new(240, IVec2::ZERO)
                .with_naighbors(3)
                .contains(&Region::new(240, IVec2::new(-3, 3))),
            "A neighbor 3 to the West and 3 to the North is within the distance of 3"
        );

        assert!(
            Region::new(240, IVec2::ZERO)
                .with_naighbors(3)
                .contains(&Region::new(240, IVec2::new(-4, 3)))
                .not(),
            "A neighbor 4 to the West and 3 to the North is further than 3"
        );

        assert!(
            Region::new(20, IVec2::new(3, 2))
                .with_naighbors(2)
                .contains(&Region::new(20, IVec2::new(5, 1))),
            "Some small, random neighbor"
        );

        assert!(
            Region::new(500, IVec2::new(3, -2))
                .with_naighbors(5)
                .contains(&Region::new(500, IVec2::new(3, -2))),
            "A region is in it's own neighborhood. Duh!"
        );
    }
}

let touches = []

export function setup_touch_events () {
  console.warn ("Setting up touch events based on JS interop.")

  const element = document.querySelector ("#game-canvas")

  element.addEventListener ('touchstart', update_touch_points)
  element.addEventListener ('touchend', update_touch_points)
  element.addEventListener ('touchcancel', update_touch_points)
  element.addEventListener ('touchmove', update_touch_points)
}

export function get_touch_points () {
  return touches
}

function update_touch_points (event) {
  event.preventDefault ()
  touches =
    Array
    .from (event.touches)
    .map ((touch) => {
      return {
        x: touch.pageX,
        y: touch.pageY,
        identifier: touch.identifier
      }
    })
}

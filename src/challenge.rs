use crate::collectibles::{Collectible, CollectiblesEvent};
use bevy::prelude::*;
use unicode_segmentation::UnicodeSegmentation;

pub struct ChallengePlugin;

impl Plugin for ChallengePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ChallengeEvent>()
            .add_startup_system(setup)
            .add_system(update);
    }
}

fn setup(mut commands: Commands, mut event_writer: EventWriter<ChallengeEvent>) {
    let challenge = Challenge::default();
    commands.insert_resource(challenge.clone());
    event_writer.send(ChallengeEvent::NewChallenge(challenge));
}

#[derive(Resource, Clone, Debug, PartialEq, Eq)]
pub enum Challenge {
    CompleteTheSententce {
        beginning: String,

        /// Part that needs to be guessed
        missing: String,

        ending: String,

        /// A list of graphemes.
        alphabet: String,

        /// What's collected so far
        collected: String,
    },
}

impl Default for Challenge {
    fn default() -> Self {
        Self::CompleteTheSententce {
            beginning: "Hello, I'm a ".to_string(),
            missing: "SNAKE".to_string(),
            ending: " who likes to read.".to_string(),
            // alphabet: "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".to_string(),
            alphabet: "A E K N S".to_string(),
            collected: String::new(),
        }
    }
}

#[derive(PartialEq, Eq)]
pub enum ChallengeEvent {
    NewChallenge(Challenge),
    CollectedCorrectly(Collectible),
    CollectedIncorrectly(Collectible),
    ChallengeComplete,
}

fn update(
    mut challenge: ResMut<Challenge>,
    mut collectibles_events: EventReader<CollectiblesEvent>,
    mut event_writer: EventWriter<ChallengeEvent>,
) {
    for CollectiblesEvent::Collected { collectible } in collectibles_events.iter() {
        match challenge.as_mut() {
            Challenge::CompleteTheSententce {
                missing,
                ref mut collected,
                ..
            } => match collectible {
                Collectible::Grapheme(grapheme) => {
                    let collection = collected.graphemes(true);
                    let Some(needed) = missing.graphemes(true).nth(collection.count()) else {
                        debug_assert_eq!(
                            collected,
                            missing,
                            "Collected is not the same as missing, but nothing is needed? That's surely a bug."
                        );
                        info!("Nothing is needed, we got whole '{collected}'.");
                        continue;
                    };
                    if grapheme == needed {
                        collected.push_str(grapheme);
                        debug!("Just collected '{grapheme}', so now we have '{collected}'.");
                        event_writer.send(ChallengeEvent::CollectedCorrectly(collectible.clone()));

                        if missing == collected {
                            event_writer.send(ChallengeEvent::ChallengeComplete);
                        }
                    } else {
                        debug!("Collected junk '{grapheme}', '{needed}' was needed.");
                        event_writer
                            .send(ChallengeEvent::CollectedIncorrectly(collectible.clone()));
                    }
                }
            },
        };
    }
}

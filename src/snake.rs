use crate::{
    configuration::Configuration,
    direction::{Direction, Heading, Shiftable},
    directives::{Directive, Directives},
    position::Position,
    protraction::{Protractable, Protraction},
    step::StepTimer,
    Layers, Systems, GRID_SIZE,
};
use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;
use std::ops::{Div, Mul, Sub};

pub struct SnakePlugin;

impl Plugin for SnakePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn)
            .add_system(
                intention
                    .label(Systems::DirectivesRead)
                    .after(Systems::DirectivesWrite),
            )
            .add_system(positioning.label(Systems::DirectivesRead).after(intention))
            .add_system(transformation.after(positioning))
            .add_system(body_shape.after(positioning));
    }
}

#[derive(Component, Debug)]
pub enum Snake {
    /// Only the head is beginning to stick out from the nest. It can't pivot,
    /// as there is no joint.
    Zero,

    /// Body is coming out. By the end of the step half of the snake will be
    /// out. It has two segments and one joint, so it can bend left or right, or
    /// just go straight.
    One { body: Protraction },

    /// The tail comes out and by the end of this step the whole snake will be
    /// visible. It has 3 segments and two joints, so there are 9 possible
    /// configurations: (straight | left | right)².
    Two {
        body: Protraction,
        tail: Protraction,
    },

    /// The fully exposed and slithering away form of the snake. This is how it
    /// will look for the most of the game. The head is thrusting forward and
    /// the tip of the tail is retracting. There are 4 segments and three
    /// pivots, so 27 possible configurations.
    Three {
        body: Protraction,
        tail: Protraction,
        tip: Protraction,
    },
}

impl Snake {
    fn default() -> Self {
        Self::Zero
    }

    fn advance(&mut self, protraction: &Protraction) -> &Self {
        info!("Advancing from {self:?} with {protraction:?}");
        *self = match self {
            Snake::Zero => Snake::One {
                body: protraction.to_owned(),
            },
            Snake::One { body } => Snake::Two {
                body: protraction.to_owned(),
                tail: *body,
            },
            Snake::Two { body, tail } => Snake::Three {
                body: protraction.to_owned(),
                tail: *body,
                tip: *tail,
            },
            Snake::Three { body, tail, tip: _ } => Snake::Three {
                body: protraction.to_owned(),
                tail: *body,
                tip: *tail,
            },
        };
        info!("Now it's {self:?}");
        self
    }
}

#[derive(Component, Debug)]
struct Intent {
    next_move: Protraction,
}

impl Default for Intent {
    fn default() -> Self {
        Self {
            next_move: Protraction::Straight,
        }
    }
}

fn spawn(mut commands: Commands, configuration: Res<Configuration>) {
    let snake = if configuration.serpentine_locomotion {
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Straight,
            tip: Protraction::Straight,
        }
    } else {
        Snake::default()
    };
    let direction = Direction::East;

    let path = if configuration.serpentine_locomotion {
        serpentine_path(0.0, &snake, &direction)
    } else {
        concertina_path(0.0, &snake, &direction)
    };

    let width = 0.3 * GRID_SIZE;

    let stroke = StrokeMode {
        color: Color::SEA_GREEN,
        options: StrokeOptions::default()
            .with_line_join(LineJoin::Round)
            .with_line_cap(LineCap::Round)
            .with_line_width(width),
    };
    let goemetry = GeometryBuilder::build_as(
        &path,
        DrawMode::Stroke(stroke),
        Transform::default().with_translation(Vec3 {
            x: 0.,
            y: 0.,
            z: 1.,
        }),
    );

    commands
        .spawn(goemetry)
        .insert(snake)
        .insert(Heading { direction })
        .insert(Position::default())
        .insert(Intent::default());
}

/// Simplified path that progresses straight forward, without "curves" like the serpentine path
fn concertina_path(step_progress: f32, snake: &Snake, direction: &Direction) -> Path {
    // Will grow from 0 to 1 in the first half and then stop.
    let first_half_progress: f32 = step_progress.min(0.5).mul(2.0).max(0.0);

    // Will stay at 0 in the first half and then grow to 1 in the second.
    let second_half_progress: f32 = step_progress.mul(2.0).sub(1.0).max(0.0);

    // Will shrink from 1 to 0 over the step duration.
    let step_regress: f32 = 1.0 - step_progress;

    // Will shrink from 1 to 0 in the first half and then stop.
    let first_half_regress: f32 = 1.0 - first_half_progress;

    // Will stay at 1 in the first half and then shrink to 0 in the second.
    let second_half_regress: f32 = 1.0 - second_half_progress;

    let sections = match snake {
        Snake::Zero => vec![(Protraction::Straight, step_progress * 4.0)],
        Snake::One { body } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (body.mirror(), 4.0),
        ],
        Snake::Two { body, tail } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (body.mirror(), 4.0),
            (tail.mirror(), 4.0),
        ],
        Snake::Three { body, tail, tip } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (body.mirror(), 4.0),
            (tail.mirror(), 4.0),
            (tip.mirror(), step_regress * 4.0),
        ],
    };

    build_path(direction, &sections)
}

/// TODO: This is behind a feature flag now. Make it work and set it as default.
fn serpentine_path(step_progress: f32, snake: &Snake, direction: &Direction) -> Path {
    // Will grow from 0 to 1 in the first half and then stop.
    let first_half_progress: f32 = step_progress.min(0.5).mul(2.0).max(0.0);

    // Will stay at 0 in the first half and then grow to 1 in the second.
    let second_half_progress: f32 = step_progress.mul(2.0).sub(1.0).max(0.0);

    // Will shrink from 1 to 0 over the step duration.
    let step_regress: f32 = 1.0 - step_progress;

    // Will shrink from 1 to 0 in the first half and then stop.
    let first_half_regress: f32 = 1.0 - first_half_progress;

    // Will stay at 1 in the first half and then shrink to 0 in the second.
    let second_half_regress: f32 = 1.0 - second_half_progress;

    let sections: Vec<(Protraction, f32)> = match snake {
        // VARIANT ZERO:
        // Just a head sticking out. Always straight.
        Snake::Zero => vec![(Protraction::Straight, step_progress * 4.0)],

        Snake::One {
            body: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            (Protraction::Right, first_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            // The middle segment
            (Protraction::Straight, first_half_progress),
            (Protraction::Left, 2.0),
            (Protraction::Left, first_half_progress),
            (Protraction::Right, 2.0),
        ],

        // VARIANT ONE:
        // Head is out, body comes out. One joint.
        Snake::One {
            body: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
        ],

        Snake::One {
            body: Protraction::Right,
        } => vec![
            // The head segment
            (Protraction::Straight, step_progress * 4.0),
            // The middle segment
            (Protraction::Straight, first_half_progress),
            (Protraction::Left, 2.0),
            (Protraction::Left, first_half_progress),
            (Protraction::Right, 2.0),
        ],

        // VARIANT TWO:
        // Head and body is out, tail comes out. Two joints.
        Snake::Two {
            body: Protraction::Straight,
            tail: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            (Protraction::Right, first_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            // The middle segment
            (Protraction::Straight, first_half_progress),
            (Protraction::Left, 2.0),
            (Protraction::Left, first_half_progress),
            (Protraction::Straight, 1.0),
            (Protraction::Right, 2.0),
            (Protraction::Right, 1.0),
            // The tail segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Right, 2.0),
        ],
        Snake::Two {
            body: Protraction::Straight,
            tail: Protraction::Left,
        } => todo!(),
        Snake::Two {
            body: Protraction::Straight,
            tail: Protraction::Right,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            (Protraction::Right, first_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            // The middle segment
            (Protraction::Straight, first_half_progress),
            (Protraction::Left, 3.0),
            (Protraction::Left, first_half_progress),
            (Protraction::Right, 1.0),
            // The tail segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Right, 2.0),
        ],
        Snake::Two {
            body: Protraction::Left,
            tail: Protraction::Straight,
        } => todo!(),
        Snake::Two {
            body: Protraction::Left,
            tail: Protraction::Left,
        } => todo!(),
        Snake::Two {
            body: Protraction::Left,
            tail: Protraction::Right,
        } => todo!(),
        Snake::Two {
            body: Protraction::Right,
            tail: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, step_progress * 4.0),
            // The middle segment
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Right, 2.0),
            (Protraction::Right, 1.0),
            // The tail segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Right, 2.0),
        ],
        Snake::Two {
            body: Protraction::Right,
            tail: Protraction::Left,
        } => todo!(),
        Snake::Two {
            body: Protraction::Right,
            tail: Protraction::Right,
        } => todo!(),

        // VARIANT THREE:
        // The whole snake is out. Head thrusts forward, tip of the tail
        // follows. Three joints. A lot of fun.
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Straight,
            tip: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            (Protraction::Right, first_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            // The middle segment
            (Protraction::Straight, first_half_progress),
            (Protraction::Left, 2.0),
            (Protraction::Left, first_half_progress),
            (Protraction::Straight, 1.0),
            (Protraction::Right, 2.0),
            (Protraction::Right, 1.0),
            // The tail segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Straight, second_half_regress),
            (Protraction::Right, 2.0),
            (Protraction::Right, second_half_regress),
            // The tip segment
            (Protraction::Straight, first_half_regress),
            (Protraction::Left, second_half_regress * 2.0),
            (Protraction::Left, first_half_regress),
            (Protraction::Right, first_half_regress * 2.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Straight,
            tip: Protraction::Left,
        } => vec![
            // The head segment
            (Protraction::Straight, step_progress * 4.0),
            // The middle segment
            (Protraction::Straight, 4.0),
            // The tail segment
            (Protraction::Straight, 4.0),
            // The tip segment
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Straight,
            tip: Protraction::Right,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            (Protraction::Right, first_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            // The middle segment
            (Protraction::Straight, first_half_progress),
            (Protraction::Left, 2.0),
            (Protraction::Left, first_half_progress),
            (Protraction::Straight, 1.0),
            (Protraction::Right, 2.0),
            (Protraction::Right, 1.0),
            // The tail segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, 2.0 + second_half_regress),
            (Protraction::Left, 1.0),
            (Protraction::Right, 1.0),
            // The tip segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, second_half_regress * 2.0),
            (Protraction::Left, first_half_regress),
            (Protraction::Right, first_half_regress * 2.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Left,
            tip: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, step_progress * 4.0),
            // The middle segment
            (Protraction::Straight, 4.0),
            // The tail segment
            (Protraction::Right, 4.0),
            // The tip segment
            (Protraction::Straight, step_regress * 4.0),
        ],

        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Left,
            tip: Protraction::Left,
        } => vec![
            // The head segment
            (Protraction::Straight, step_progress * 4.0),
            // The middle segment
            (Protraction::Straight, 4.0),
            // The tail segment
            (Protraction::Right, 4.0),
            // The tip segment
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Left,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Right,
            tip: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            (Protraction::Right, first_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            // The middle segment
            (Protraction::Left, step_regress * 2.0),
            (Protraction::Right, 1.0),
            (Protraction::Left, 2.0 + second_half_progress),
            (Protraction::Left, 1.0),
            (Protraction::Right, first_half_progress),
            // The tail segment
            (Protraction::Straight, second_half_progress),
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Right, 2.0),
            // The tip segment
            (Protraction::Right, first_half_regress),
            (Protraction::Left, second_half_regress * 2.0),
            (Protraction::Left, first_half_regress),
            (Protraction::Right, first_half_regress * 2.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Right,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Straight,
            tail: Protraction::Right,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Straight,
            tip: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, step_progress * 4.0),
            // The middle segment
            (Protraction::Right, 4.0),
            // The tail segment
            (Protraction::Straight, 4.0),
            // The tip segment
            (Protraction::Straight, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Straight,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Straight,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Left,
            tip: Protraction::Straight,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Straight, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Left,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Left,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Right,
            tip: Protraction::Straight,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Right,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Left,
            tail: Protraction::Right,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Straight,
            tip: Protraction::Straight,
        } => vec![
            // The head segment
            (Protraction::Straight, second_half_progress * 2.0),
            (Protraction::Right, second_half_progress),
            (Protraction::Left, first_half_progress * 2.0),
            (Protraction::Left, second_half_progress),
            // The middle segment
            (Protraction::Straight, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Right, 2.0),
            (Protraction::Right, 1.0),
            // The tail segment
            (Protraction::Straight, 1.0),
            (Protraction::Left, 2.0),
            (Protraction::Left, 1.0),
            (Protraction::Straight, second_half_regress),
            (Protraction::Right, 2.0),
            (Protraction::Right, second_half_regress),
            // The tip segment
            (Protraction::Straight, first_half_regress),
            (Protraction::Left, second_half_regress * 2.0),
            (Protraction::Left, first_half_regress),
            (Protraction::Right, first_half_regress * 2.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Straight,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Straight,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Straight, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Left,
            tip: Protraction::Straight,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Straight, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Left,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Left,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Right, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Right,
            tip: Protraction::Straight,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Straight, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Right,
            tip: Protraction::Left,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Right, step_regress * 4.0),
        ],
        Snake::Three {
            body: Protraction::Right,
            tail: Protraction::Right,
            tip: Protraction::Right,
        } => vec![
            (Protraction::Straight, step_progress * 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Left, 4.0),
            (Protraction::Left, step_regress * 4.0),
        ],
    };

    build_path(direction, &sections)
}

fn build_path(orientation: &Direction, sections: &Vec<(Protraction, f32)>) -> Path {
    let mut path_builder = PathBuilder::new();
    let mut point = Vec2::ZERO;
    let mut direction = orientation.opposite();
    path_builder.move_to(point);

    for (protraction, distance) in sections.iter() {
        direction = direction.protract(protraction);
        let shift = Vec2::from(&direction)
            .mul(*distance)
            .mul(GRID_SIZE)
            .div(4.0);
        point = point + shift;
        path_builder.line_to(point);
    }
    path_builder.build()
}

/// Shape up the snake based on the body description
fn body_shape(
    mut snakes: Query<(&Snake, &Heading, &mut Path)>,
    timer: Res<StepTimer>,
    configuration: Res<Configuration>,
) {
    let (snake, heading, mut path) = snakes.get_single_mut().unwrap();
    let progress = timer.percent();

    *path = if configuration.serpentine_locomotion {
        serpentine_path(progress, &snake, &heading.direction)
    } else {
        concertina_path(progress, &snake, &heading.direction)
    };
}

/// Change where the snake is heading based on user directives
fn intention(mut snakes: Query<(&mut Intent, &Heading), With<Snake>>, directives: Res<Directives>) {
    let (mut intent, heading) = snakes.get_single_mut().unwrap();

    for directive in directives.iter() {
        match directive {
            Directive::Move(direction) => {
                if let Some(protraction) = direction.protraction_of(&heading.direction) {
                    debug!("Will turn {protraction}.");
                    intent.next_move = protraction;
                }
            }
        };
    }
}

/// Update the visual position
///
/// This happens on every frame
fn transformation(mut snakes: Query<(&Heading, &mut Transform, &Position)>, timer: Res<StepTimer>) {
    let (heading, mut transform, position) = snakes.get_single_mut().unwrap();
    let translation = Vec2::from(position) + Vec2::from(&heading.direction) * timer.percent();

    transform.translation = translation.mul(GRID_SIZE).extend(Layers::SNAKE);
}

/// Update the snake logical position on the grid one step ahead
///
/// This happens on every [step](crate::step)
fn positioning(
    mut snakes: Query<(&mut Position, &mut Heading, &mut Intent, &mut Snake)>,
    timer: Res<StepTimer>,
) {
    // TODO: Consider using step::newly_commenced run criterion. Somehow it
    // makes the snake shiver. It probably has something to do with systems
    // ordering.
    if !timer.just_finished() {
        return;
    };

    let (mut position, mut heading, mut intent, mut snake) = snakes.get_single_mut().unwrap();

    position.shift(&heading.direction);
    heading.direction = heading.direction.protract(&intent.next_move);
    snake.advance(&intent.next_move);
    intent.next_move = Protraction::Straight;
}

use crate::Systems;
use bevy::{input::touch::Touch, prelude::*};
use wasm_bindgen::prelude::*;

pub struct TouchPlugin;

impl Plugin for TouchPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<TouchPoints>()
            .add_startup_system(setup)
            .add_system(touch.label(Systems::Input));
    }
}

#[derive(Resource, Default, Debug)]
pub struct TouchPoints(pub Vec<TouchPoint>);

impl TouchPoints {
    pub fn get(&self, identifier: i64) -> Option<&TouchPoint> {
        self.0.iter().find(|touch| touch.identifier == identifier)
    }

    pub fn one(&self) -> Option<&TouchPoint> {
        self.0.first()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct TouchPoint {
    pub translation: Vec2,
    pub identifier: i64,
}

impl From<&Touch> for TouchPoint {
    fn from(value: &Touch) -> Self {
        Self {
            identifier: value.id() as i64,
            translation: value.position(),
        }
    }
}

#[cfg(not(target_arch = "wasm32"))]
fn setup() {
    // Nothing to do on native targets. We are all set!
}

#[cfg(not(target_arch = "wasm32"))]
fn touch(touches: Res<Touches>, mut points: ResMut<TouchPoints>) {
    // TODO: Translate native touches into touchpoints

    points.0 = touches.iter().map(TouchPoint::from).collect();
}

// JS INTEROP

#[cfg(target_arch = "wasm32")]
fn setup() {
    setup_touch_events();
}

#[cfg(target_arch = "wasm32")]
fn touch(mut points: ResMut<TouchPoints>, windows: Res<Windows>) {
    let window = windows.primary();
    points.0 = get_touch_points()
        .iter()
        .map(JsTouchPoint::from)
        .map(|jstouch| TouchPoint::from_js(jstouch, window))
        .collect();
}

/// This is a raw JS touch point.
///
/// It is extracted from a [JS Touch] value. Most notably it's coordinate system
/// is different than the one used in internal `TouchPoint` struct:
///
/// 1. The origin (0, 0) is in the top left corner of the window.
///
/// 2. The Y axis is downward.
///
/// [JS Touch]: https://developer.mozilla.org/en-US/docs/Web/API/Touch
#[cfg(target_arch = "wasm32")]
#[derive(Debug)]
struct JsTouchPoint {
    x: f64,
    y: f64,
    identifier: i64,
}

#[cfg(target_arch = "wasm32")]
impl From<&JsValue> for JsTouchPoint {
    fn from(value: &JsValue) -> Self {
        let x_key = js_sys::JsString::from("x");
        let y_key = js_sys::JsString::from("y");
        let identifier_key = js_sys::JsString::from("identifier");

        let x = js_sys::Reflect::get(value, &x_key)
            .unwrap()
            .as_f64()
            .unwrap();
        let y = js_sys::Reflect::get(value, &y_key)
            .unwrap()
            .as_f64()
            .unwrap();
        let identifier = js_sys::Reflect::get(value, &identifier_key)
            .unwrap()
            .as_f64()
            .unwrap() as i64;
        // let identifier = "js_sys::Reflect::get(&value, &identifier_key).unwrap().as_string().unwrap()".to_string();
        Self { x, y, identifier }
    }
}

impl TouchPoint {
    #[cfg(target_arch = "wasm32")]
    fn from_js(js_touch: JsTouchPoint, window: &Window) -> Self {
        let x = js_touch.x as f32 - window.width() / 2.;
        let y = -(js_touch.y as f32 - window.height() / 2.);

        Self {
            identifier: js_touch.identifier,
            translation: Vec2 { x, y },
        }
    }
}

#[wasm_bindgen(module = "/src/touch.js")]
extern "C" {
    fn setup_touch_events();
    fn get_touch_points() -> Vec<JsValue>;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_touch_point_by_id() {
        let points = TouchPoints(vec![
            TouchPoint {
                identifier: 0,
                translation: Vec2 { x: 0.0, y: 0.0 },
            },
            TouchPoint {
                identifier: 1,
                translation: Vec2 { x: 1.0, y: 0.0 },
            },
            TouchPoint {
                identifier: 2,
                translation: Vec2 { x: 2.0, y: 0.0 },
            },
        ]);

        let Some(extracted) = TouchPoints::get(&points, 0) else { panic!("Touch point 0 should be there.") };
        assert!(
            extracted
                .translation
                .distance(Vec2::new(0.0, 0.0))
                .lt(&0.01),
            "Touch point 0 should be at ( 0, 0 ). Got {extracted:?}"
        );

        let Some(extracted) = points.get(1) else { panic!("Touch point 1 should be there.") };
        assert!(
            extracted
                .translation
                .distance(Vec2::new(1.0, 0.0))
                .lt(&0.01),
            "Touch point 1 should be at ( 1, 0 ). Got {extracted:?}"
        );

        let Some(extracted) = points.get(2) else { panic!("Touch point 2 should be there.") };
        assert!(
            extracted
                .translation
                .distance(Vec2::new(2.0, 0.0))
                .lt(&0.01),
            "Touch point 2 should be at ( 2, 0 ). Got {extracted:?}"
        );

        if let Some(extracted) = points.get(3) {
            panic!("Touch point 3 should not be there. Got {extracted:?}")
        };
    }
}

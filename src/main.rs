mod annotations;
mod flame;
mod background;
mod camera;
mod challenge;
mod collectibles;
mod configuration;
mod diagnostics;
mod direction;
mod directives;
mod keyboard;
mod life_cycle;
mod position;
mod protraction;
mod region;
mod snake;
mod step;
mod swipe;
mod touch;

use annotations::AnnotationsPlugin;
use background::BackgroundPlugin;
use bevy::{log::LogPlugin, prelude::*};
use bevy_particle_systems::ParticleSystemPlugin;
use bevy_prototype_lyon::prelude::*;
use camera::CameraPlugin;
use challenge::ChallengePlugin;
use collectibles::CollectiblesPlugin;
use configuration::Configuration;
use diagnostics::DiagnosticsPlugin;
use directives::DirectivesPlugin;
use keyboard::KeyboardPlugin;
use life_cycle::LifeCyclePlugin;
use snake::SnakePlugin;
use step::StepPlugin;
use swipe::SwipePlugin;
use touch::TouchPlugin;
use flame::FlamePlugin;

/// The size of one grid unit
pub const GRID_SIZE: f32 = 100.;

/// This represents different layers (z-indices) in the game. Higher number are in front of lower.
pub struct Layers;

impl Layers {
    pub const GROUND: f32 = -3.0;
    pub const ANNOTATIONS: f32 = -2.0;
    pub const COLLECTIBLES: f32 = -1.0;
    pub const SNAKE: f32 = 0.0;
    pub const FLAME: f32 = 1.0;
    pub const AIR: f32 = 10.0;
    pub const CAMERA: f32 = 100.0;
}

fn main() {
    let configuration = Configuration::init();
    App::new()
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(ClearColor(Color::DARK_GRAY))
        .insert_resource(configuration.clone())
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    window: WindowDescriptor {
                        title: "Bevy Lyon Playground".into(),
                        canvas: Some("#game-canvas".into()),
                        fit_canvas_to_parent: true,
                        ..default()
                    },
                    ..default()
                })
                .set(LogPlugin {
                    // TODO: Implement a cross-platform way to toggle it at runtime.
                    level: if configuration.debug {
                        bevy::log::Level::DEBUG
                    } else {
                        bevy::log::Level::INFO
                    },
                    ..default()
                }),
        )
        .add_system(bevy::window::close_on_esc)
        .add_plugin(ParticleSystemPlugin::default())
        .add_plugin(CameraPlugin)
        .add_plugin(BackgroundPlugin)
        .add_plugin(ShapePlugin)
        .add_plugin(DirectivesPlugin)
        .add_plugin(KeyboardPlugin)
        .add_plugin(TouchPlugin)
        .add_plugin(SwipePlugin)
        .add_plugin(DiagnosticsPlugin)
        .add_plugin(AnnotationsPlugin)
        .add_plugin(ChallengePlugin)
        .add_plugin(StepPlugin)
        .add_plugin(SnakePlugin)
        .add_plugin(CollectiblesPlugin)
        .add_plugin(LifeCyclePlugin)
        .add_plugin(FlamePlugin)
        .run();
}

#[derive(SystemLabel, Debug, Clone, PartialEq, Eq, Hash)]
enum Systems {
    /// This label marks system that resets all directives
    ///
    /// This system should always be run at the beginning of the frame.
    ///
    /// If you encounter problems with input (like it's not handled) make sure
    /// that any system that writes directives or reads them doesn't run before directives reset.
    DirectivesReset,

    /// Use this label for anything that takes input.
    Input,

    /// Use this label for any system that issues directives
    ///
    /// It often goes together with Input label, but not always. For example
    /// touch is only processing user input while swipe is only issueing
    /// directives based on this input.
    DirectivesWrite,

    /// Use this label for any system that acts upon directives written by other systems.
    ///
    /// This systems should ran last.
    DirectivesRead,
}

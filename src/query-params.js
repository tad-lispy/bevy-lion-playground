// This are helper functions to be used via wesm_bindgen

const params = new URLSearchParams (location.search)

export function get_query_param (name) {
  let value = params.get(name)
  // Support shorthand, boolean params, like debug in ?debug&scale=5
  if (value === "") value = "true"
  return value
}

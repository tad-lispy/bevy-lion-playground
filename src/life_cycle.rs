use bevy::prelude::*;

pub struct LifeCyclePlugin;

impl Plugin for LifeCyclePlugin {
    fn build(&self, app: &mut App) {
        app.add_system(progress);
    }
}


#[derive(Component, Debug)]
pub enum LifeCycle {
    Growing(Timer),
    Mature,
    Decaying(Timer),
    Collected(Timer),
}

impl LifeCycle {
    pub fn growing(duration: f32) -> Self {
        LifeCycle::Growing(Timer::from_seconds(duration, TimerMode::Once))
    }

    pub fn decaying(duration: f32) -> Self {
        LifeCycle::Decaying(Timer::from_seconds(duration, TimerMode::Once))
    }

    pub fn collected(duration: f32) -> Self {
        LifeCycle::Collected(Timer::from_seconds(duration, TimerMode::Once))
    }
}


fn progress(
    mut query: Query<(Entity, &mut LifeCycle)>,
    mut commands: Commands,
    time: Res<Time>,
) {
    for (entity, mut life_cycle) in query.iter_mut() {
        match life_cycle.as_mut() {
            LifeCycle::Growing(timer) => {
                timer.tick(time.delta());
                if timer.finished() {
                    debug!("Entity is mature.");
                    *life_cycle = LifeCycle::Mature;
                }
            }
            LifeCycle::Mature => {
                // Nothing to do. It just sits there.
            }
            LifeCycle::Decaying(timer) => {
                timer.tick(time.delta());
                if timer.finished() {
                    debug!("Despawning the decayed collectible.");
                    commands.entity(entity).despawn();
                }
            }
            LifeCycle::Collected(timer) => {
                timer.tick(time.delta());
                if timer.finished() {
                    debug!("Despawning the collected object.");
                    commands.entity(entity).despawn();
                }
            }
        }
    }
}

use crate::direction::Direction;
use crate::Systems;
use bevy::{prelude::*, utils::HashSet};

pub struct DirectivesPlugin;

impl Plugin for DirectivesPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Directives>().add_system(
            reset
                .label(Systems::DirectivesReset)
                .before(Systems::Input)
                .before(Systems::DirectivesWrite)
                .before(Systems::DirectivesRead),
        );
    }
}

#[derive(PartialEq, Eq, Hash, Debug)]
pub enum Directive {
    Move(Direction),
}

#[derive(Resource, Deref, DerefMut, Default)]
pub struct Directives(HashSet<Directive>);


fn reset(mut directives: ResMut<Directives>) {
    directives.clear();
}

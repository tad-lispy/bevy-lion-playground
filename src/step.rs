use crate::configuration::Configuration;
use bevy::{prelude::*, ecs::schedule::ShouldRun};

pub struct StepPlugin;

#[derive(Resource, Deref, DerefMut)]
pub struct StepTimer {
    timer: Timer,
}

impl Plugin for StepPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup)
            .add_system(apply_configuration.with_run_criteria(newly_commenced))
            .add_system(progress);
    }
}

/// A run criterion that indicates that it's time to take another step
pub fn newly_commenced(timer: Res<StepTimer>) -> ShouldRun {
    timer.just_finished().into()
}

fn setup(mut commands: Commands, configuration: Res<Configuration>) {
    commands.insert_resource(StepTimer {
        timer: Timer::new(configuration.step_duration(), TimerMode::Repeating),
    })
}

fn progress(time: Res<Time>, mut timer: ResMut<StepTimer>) {
    timer.tick(time.delta());
}

fn apply_configuration(mut timer: ResMut<StepTimer>, configuration: Res<Configuration>) {
    let duration = configuration.step_duration();
    if duration == timer.duration() {
        return;
    };

    debug!("Setting step duration to {duration:?}");
    timer.set_duration(configuration.step_duration())
}

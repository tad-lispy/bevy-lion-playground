use crate::{
    challenge::Challenge, configuration::Configuration, life_cycle::LifeCycle, position::Position,
    snake::Snake, step, Layers, GRID_SIZE,
};
use bevy::{prelude::*, utils::HashMap};
use itertools::Itertools;
use rand::{rngs::ThreadRng, seq::IteratorRandom, Rng};
use std::ops::Mul;
use unicode_segmentation::UnicodeSegmentation;

pub struct CollectiblesPlugin;

impl Plugin for CollectiblesPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<CollectiblesIndex>()
            .add_event::<CollectiblesEvent>()
            .add_system(animate)
            .add_system(spawn.with_run_criteria(step::newly_commenced))
            .add_system(decay.with_run_criteria(step::newly_commenced))
            .add_system(
                collect
                    .with_run_criteria(step::newly_commenced)
                    .after(spawn)
                    .after(decay),
            );
    }
}

/// An event sent when something is collected
pub enum CollectiblesEvent {
    Collected { collectible: Collectible },
}

#[derive(Component, Debug, Eq, PartialEq, Clone)]
pub enum Collectible {
    Grapheme(String),
}

#[derive(Resource, Default, Debug)]
pub struct CollectiblesIndex {
    entities: HashMap<Position, Entity>,
}

#[derive(Bundle)]
pub struct CollectibleBundle {
    pub collectible: Collectible,
    pub position: Position,
    pub life_cycle: LifeCycle,
}

fn spawn(
    mut commands: Commands,
    snakes: Query<&Position, With<Snake>>,
    asset_server: Res<AssetServer>,
    mut index: ResMut<CollectiblesIndex>,
    challenge: Res<Challenge>,
    configuration: Res<Configuration>,
) {
    // TODO: Move the RNG to a shared resource?
    let mut rng = rand::thread_rng();

    let probability = 0.3;
    if rng.gen::<f32>() > probability {
        return;
    };

    let center = if let Ok(position) = snakes.get_single() {
        position.to_owned()
    } else {
        Position::default()
    };
    let distance = 20;
    let position = Position::random(&mut rng, center, distance);

    if index.entities.contains_key(&position) {
        debug!("There is already a collectible at {position:?}. Spawn aborted.");
        return;
    };

    let collectible = match challenge.into_inner() {
        Challenge::CompleteTheSententce { alphabet, .. } => {
            let grapheme = random_grapheme(&mut rng, &alphabet);
            Collectible::Grapheme(grapheme.clone())
        }
    };

    debug!("Spawning {collectible:?} at {position:?}");

    let bundle = match collectible.clone() {
        Collectible::Grapheme(grapheme) => {
            let font = asset_server.load("fonts/Cantarell-Bold.ttf");
            let style = TextStyle {
                color: Color::WHITE,
                font,
                font_size: 150.0 * configuration.text_upscale,
            };
            let alignment = TextAlignment::CENTER;

            Text2dBundle {
                text: Text::from_section(grapheme, style).with_alignment(alignment),
                transform: Transform::from_translation(
                    Vec2::from(&position)
                        .mul(GRID_SIZE)
                        .extend(Layers::COLLECTIBLES),
                )
                .with_scale(Vec3::ZERO),
                ..default()
            }
        }
    };

    let entity = commands
        .spawn(bundle)
        .insert(CollectibleBundle {
            collectible,
            position: position.to_owned(),
            life_cycle: LifeCycle::growing(0.5),
        })
        .id();

    index.entities.insert(position, entity);
}

fn animate(
    mut collectibles: Query<
        (&LifeCycle, &mut Transform, &mut Text),
        (Changed<LifeCycle>, With<Collectible>),
    >,
    configuration: Res<Configuration>,
) {
    for (life_cycle, mut transform, mut text) in collectibles.iter_mut() {
        match life_cycle {
            LifeCycle::Growing(timer) => {
                let scale = timer.percent() / configuration.text_upscale;
                transform.scale = Vec3::splat(scale);
            }
            LifeCycle::Mature => {
                transform.scale = Vec3::ONE / configuration.text_upscale;
            }
            LifeCycle::Decaying(timer) => {
                let scale = timer.percent_left() / configuration.text_upscale;
                transform.scale = Vec3::splat(scale);
            }
            LifeCycle::Collected(timer) => {
                let multiplier = 20.0;
                let scale = 1.0 + multiplier / configuration.text_upscale * timer.percent();
                transform.scale = Vec3::splat(scale);
                transform.translation.z = Layers::AIR;
                for section in text.bypass_change_detection().sections.iter_mut() {
                    section.style.color.set_a(timer.percent_left().powi(2));
                }
            }
        }
    }
}

fn collect(
    mut commands: Commands,
    mut snakes: Query<&Position, With<Snake>>,
    collectibles: Query<&Collectible>,
    mut index: ResMut<CollectiblesIndex>,
    mut event_writer: EventWriter<CollectiblesEvent>,
) {
    let Ok(position) = snakes.get_single_mut() else {
        debug!("There is no snake to collect anything.");
        return
    };

    let Some(entity) = index.entities.get(position) else {
        debug!("Nothing to collect at {position:?}");
        return
    };

    let Ok(collectible) = collectibles.get(*entity) else {
        // This is really an error, but maybe there is no need to panic.
        error!("There is no collectible at {position:?}, but the index indicates there is.");
        return
    };

    debug!("Collected {collectible:?}");
    event_writer.send(CollectiblesEvent::Collected {
        collectible: collectible.clone(),
    });
    commands.entity(*entity).insert(LifeCycle::collected(0.75));
    index.entities.remove(position);
}

fn decay(mut commands: Commands, mut index: ResMut<CollectiblesIndex>) {
    let probability = 0.005;
    let mut rng = rand::thread_rng();

    for (position, entity) in index.entities.clone().iter() {
        if rng.gen::<f32>() > probability {
            continue;
        };

        commands.entity(*entity).insert(LifeCycle::decaying(1.0));
        index.entities.remove(&position);
    }
}

/// Get a random grapheme from a string. Will panic if the string doesn't contain any words.
fn random_grapheme(rng: &mut ThreadRng, alphabet: &str) -> String {
    let graphemes = alphabet
        .unicode_words()
        .flat_map(|word| word.graphemes(true))
        .dedup();

    graphemes
        .choose(rng)
        .expect("The alphabet has to contain some words.")
        .to_string()
}

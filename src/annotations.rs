use crate::{
    challenge::{Challenge, ChallengeEvent},
    configuration::Configuration,
    Layers,
};
use bevy::prelude::*;
use unicode_segmentation::UnicodeSegmentation;

pub struct AnnotationsPlugin;

impl Plugin for AnnotationsPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ActiveAnnotation>()
            .add_system(update_annotations)
            .add_system(update_current_annotation)
            .add_system(reaveal_annotations);
    }
}

#[derive(Resource, Default, Debug)]
pub struct ActiveAnnotation {
    annotation: Option<Entity>,
}

/// Tags an annotation entity and keeps track of sections visibility
#[derive(Component, Default, Debug)]
struct Annotation {
    /// Timers are used to animate revealing of individual graphemes in the
    /// annotation.  Each timer corresponds to a single text section. If there
    /// is no timer for a given section, it is assumed to be at 0.
    timers: Vec<Timer>,
}
impl Annotation {
    fn reveale_next_section(&mut self) -> &Self {
        self.timers.push(Timer::from_seconds(2.0, TimerMode::Once));
        self
    }
}

fn update_annotations(mut annotations: Query<&mut Annotation>, time: Res<Time>) {
    for mut annotation in annotations.iter_mut() {
        for timer in annotation.timers.iter_mut() {
            timer.tick(time.delta());
        }
    }
}

fn update_current_annotation(
    mut annotations: Query<&mut Annotation>,
    mut commands: Commands,
    mut challenge_events: EventReader<ChallengeEvent>,
    mut active_annotation: ResMut<ActiveAnnotation>,
    asset_server: Res<AssetServer>,
    configuration: Res<Configuration>,
) {
    for event in challenge_events.iter() {
        match event {
            ChallengeEvent::NewChallenge(challenge) => {
                let text = match challenge {
                    Challenge::CompleteTheSententce { missing, .. } => missing,
                };

                debug!("Inserting new annotation: '{text}'");

                let font = asset_server.load("fonts/Cantarell-Bold.ttf");

                // Rendering text with very high font size makes the game hiccup. These
                // values were chosen to provide good compromise between crispy edges and
                // runtime performance.
                let font_size = 150.0 * configuration.text_upscale;
                let font_scale = Vec3::ONE * 16.0 / configuration.text_upscale;

                let style = TextStyle {
                    color: Color::hsla(0.0, 0.0, 0.16, 0.0),
                    font,
                    font_size,
                };

                let alignment = TextAlignment::CENTER;

                let translation = Vec2::ZERO.extend(Layers::ANNOTATIONS);

                let sections = text.graphemes(true).map(|grapheme| TextSection {
                    value: grapheme.to_string(),
                    style: style.clone(),
                });
                let annotation_id = commands
                    .spawn(Text2dBundle {
                        text: Text::from_sections(sections).with_alignment(alignment),
                        transform: Transform::from_translation(translation).with_scale(font_scale),
                        ..default()
                    })
                    .insert(Annotation::default())
                    .id();
                active_annotation.annotation = Some(annotation_id);
            }
            ChallengeEvent::CollectedCorrectly(collectible) => {
                let Some(annotation_id) = active_annotation.annotation else {
                    panic!("A challenge event indicates correct collection ({collectible:?}), but there is no active annotation to update!");
                };

                let Ok(mut annotation) = annotations.get_mut(annotation_id) else {
                    panic!("A challenge event indicates correct collection ({collectible:?}), but the active annotation doesn't point to an annotation entity!");
                };

                annotation.reveale_next_section();
            }
            ChallengeEvent::CollectedIncorrectly(_) => {
                // Nothing to do here.
                // TODO: Consider splitting this system into two: spawn and reveal
            }
            ChallengeEvent::ChallengeComplete => {
                // Nothing to do here.
            },
        }
    }
}

fn reaveal_annotations(mut annotations: Query<(&mut Text, &Annotation)>) {
    for (mut text, annotation) in annotations.iter_mut() {
        for (index, section) in text
            .bypass_change_detection()
            .sections
            .iter_mut()
            .enumerate()
        {
            if let Some(timer) = annotation.timers.get(index) {
                let alpha = timer.percent();
                section.style.color.set_a(alpha);
            }
        }
    }
}

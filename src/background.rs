use crate::{
    region::{InRegion, Region},
    Layers,
};
use bevy::{prelude::*, utils::HashMap};

/// The length of the image in pixels. It must be a square. If you change the image, change this constant to match.
const IMAGE_SIZE: u32 = 500;

/// How much to scale the image.
const SCALING_FACTOR: u32 = 10;

/// How many neighbors to spawn around the camera
const NEIGHBORHOOD_SIZE: u8 = 3;

pub struct BackgroundPlugin;

impl Plugin for BackgroundPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Tiles>().add_system(spawn_tiles);
    }
}

#[derive(Resource, Default)]
struct Tiles {
    entities: HashMap<Region, Entity>,
}

fn spawn_tiles(
    cameras: Query<&Transform, With<Camera>>,
    mut commands: Commands,
    mut tiles: ResMut<Tiles>,
    asset_server: Res<AssetServer>,
) {
    let tile_size = IMAGE_SIZE * SCALING_FACTOR;

    let camera = cameras.get_single().unwrap();
    let regions = camera
        .translation
        .get_region(tile_size)
        .with_naighbors(NEIGHBORHOOD_SIZE);
    for region in regions {
        if tiles
            .entities
            .get(&region)
            .and_then(|entity| commands.get_entity(*entity))
            .is_some()
        {
            // There is already a tile for this region
            continue;
        }

        debug!("Spawning a new background tile for region {region:?}");
        let image = asset_server.load("cartographer.png");
        let entity = commands
            .spawn(SpriteBundle {
                texture: image,
                transform: Transform {
                    scale: Vec2::splat(10.0).extend(0.0),
                    translation: Vec2::from(region).extend(Layers::GROUND),
                    rotation: Quat::IDENTITY,
                },
                ..default()
            })
            .id();
        tiles.entities.insert(region, entity);
    }
}

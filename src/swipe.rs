use crate::{
    configuration::Configuration,
    direction::Direction,
    directives::{Directive, Directives},
    step::StepTimer,
    touch::TouchPoints,
    Systems,
};
use bevy::prelude::*;

/// Below this length, swipes will be ignored.
pub struct SwipePlugin;

impl Plugin for SwipePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Swipes>().add_system(
            swiping
                .label(Systems::DirectivesWrite)
                .after(Systems::Input),
        );
    }
}

#[derive(Resource, Default, Debug, Clone, Copy)]
struct Swipes(Option<Swipe>);

impl Swipes {
    fn track(&mut self, id: i64, position: Vec2) {
        // TODO: Use get and some Option wizardry to make it short and clever?
        if let Some(ref mut swipe) = self.0 {
            if swipe.id == id {
                swipe.to = position;
            }
        } else {
            let new_swipe = Swipe::new(id, position);
            self.0 = Some(new_swipe)
        }
    }

    fn anchor(&mut self, id: i64, position: Vec2) {
        // TODO: Use get and some Option wizardry to make it short and clever?
        if let Some(ref mut swipe) = self.0 {
            if swipe.id == id {
                swipe.from = position;
            }
        }
    }

    fn direction(self, sensitivity: f32) -> Option<Direction> {
        self.0.and_then(|swipe| swipe.direction(sensitivity))
    }
}

#[derive(Debug, Clone, Copy)]
struct Swipe {
    id: i64,
    from: Vec2,
    to: Vec2,
}

impl Swipe {
    fn new(id: i64, position: Vec2) -> Self {
        Self {
            id,
            from: position,
            to: position,
        }
    }

    fn direction(self, sensitivity: f32) -> Option<Direction> {
        // How long does the swipe need to be (in pixels) to be conclusive
        let threshold = 1.0 / sensitivity;
        let displacement = self.to - self.from;
        if displacement.length() < threshold {
            return None;
        }

        let [x, y] = displacement.to_array();
        if x.abs() > y.abs() {
            if x > 0.0 {
                Some(Direction::East)
            } else {
                Some(Direction::West)
            }
        } else {
            if y > 0.0 {
                Some(Direction::North)
            } else {
                Some(Direction::South)
            }
        }
    }
}

fn swiping(
    mut swipes: ResMut<Swipes>,
    mut directives: ResMut<Directives>,
    touchpoints: Res<TouchPoints>,
    step: Res<StepTimer>,
    configuration: Res<Configuration>,
) {
    // We only care about one touch point. Multi touch gestures are not supported.

    if let Some(finger) = touchpoints.one() {
        debug!(
            "Tracking the swipe {identifier:?} at {position:?}",
            identifier = finger.identifier,
            position = finger.translation
        );

        swipes.track(finger.identifier, finger.translation);

        if step.just_finished() {
            if let Some(direction) = swipes.direction(configuration.touch_sensitivity) {
                debug!("Re-anchoring the swipe and issueing a directive to move {direction:?}");
                directives.insert(Directive::Move(direction));
            } else {
                debug!(
                    "Swipe direction was inconclusive. Re-anchoring the swipe at {anchor:?}.",
                    anchor = finger.translation
                );
            }
            swipes.anchor(finger.identifier, finger.translation);
        }
    } else {
        if let Some(direction) = swipes.direction(configuration.touch_sensitivity) {
            debug!("Swipe is over. Issuing a directive to move {direction:?}");
            directives.insert(Directive::Move(direction));
        } else {
            debug!("Swipe is over, but it's direction was inconclusive. Aborting.");
        }
        swipes.0 = None;
    }
}

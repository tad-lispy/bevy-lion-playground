use crate::{configuration::Configuration, snake::Snake, Layers};
use bevy::prelude::*;

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_startup_system(setup_camera)
            .add_system(follow_the_snake);
    }
}

fn setup_camera(mut commands: Commands, configuration: Res<Configuration>) {
    commands.spawn(Camera2dBundle {
        projection: OrthographicProjection {
            scale: configuration.scale,
            ..default()
        },
        ..default()
    });
}

fn follow_the_snake(
    mut cameras: Query<&mut Transform, With<Camera2d>>,
    snakes: Query<&Transform, (With<Snake>, Without<Camera2d>)>,
) {
    let mut camera = cameras.single_mut();
    let Ok(transform) = snakes.get_single() else {
        debug!("There is no snake in this game. Nothing to track.");
        return
    };

    camera.translation = transform.translation + Vec3::Z * Layers::CAMERA;
}

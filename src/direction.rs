use bevy::prelude::*;
use rand::prelude::Distribution;

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}
impl Direction {
    pub fn opposite(&self) -> Self {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }
}


// A Component wrapper for Direction.
//
// Enums as Components seem to be difficult to work with.
#[derive(Component, Debug, PartialEq, Eq)]
pub struct Heading {
    pub direction: Direction,
}

pub trait Shiftable {
    fn shift(&mut self, direction: &Direction) -> &mut Self;
}

impl Shiftable for IVec2 {
    fn shift(&mut self, direction: &Direction) -> &mut Self {
        *self += IVec2::from(direction);
        self
    }
}

impl From<&Direction> for Vec2 {
    fn from(value: &Direction) -> Self {
        match value {
            Direction::North => Vec2::Y,
            Direction::East => Vec2::X,
            Direction::South => Vec2::NEG_Y,
            Direction::West => Vec2::NEG_X,
        }
    }
}

impl From<&Direction> for IVec2 {
    fn from(value: &Direction) -> Self {
        match value {
            Direction::North => IVec2::Y,
            Direction::East => IVec2::X,
            Direction::South => IVec2::NEG_Y,
            Direction::West => IVec2::NEG_X,
        }
    }
}

impl Distribution<Direction> for rand::distributions::Standard {
    fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Direction {
        match rng.gen_range(0..=3) {
            0 => Direction::North,
            1 => Direction::East,
            2 => Direction::South,
            _ => Direction::West,
        }
    }
}

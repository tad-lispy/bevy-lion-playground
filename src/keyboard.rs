use crate::{
    direction::Direction,
    directives::{Directive, Directives},
    Systems,
};
use bevy::prelude::*;

pub struct KeyboardPlugin;

impl Plugin for KeyboardPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(
            input
                .label(Systems::Input)
                .label(Systems::DirectivesWrite),
        );
    }
}

fn input(keyboard: Res<Input<KeyCode>>, mut directives: ResMut<Directives>) {
    if keyboard.pressed(KeyCode::Up) {
        directives.insert(Directive::Move(Direction::North));
    }

    if keyboard.pressed(KeyCode::Down) {
        directives.insert(Directive::Move(Direction::South));
    }

    if keyboard.pressed(KeyCode::Left) {
        directives.insert(Directive::Move(Direction::West));
    }

    if keyboard.pressed(KeyCode::Right) {
        directives.insert(Directive::Move(Direction::East));
    }
}

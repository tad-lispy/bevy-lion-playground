use bevy::prelude::*;
use bevy_particle_systems::*;
use std::{f32::consts::PI, ops::Mul};

use crate::{
    challenge::ChallengeEvent, configuration::Configuration, position::Position, Layers, GRID_SIZE,
};

pub struct FlamePlugin;

impl Plugin for FlamePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup)
            .add_system(update_shape)
            .add_system(extinguish);
    }
}

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    configuration: Res<Configuration>,
) {
    // TODO: Remove the feature flag once flame system is ready
    if !configuration.flame {
        return;
    };

    info!("Starting flames");

    const SIZE: i32 = 15;

    let lines = vec![
        (Position::new(SIZE, 0), 0.0),
        (Position::new(0, SIZE), PI / 2.0),
        (Position::new(-SIZE, 0), PI),
        (Position::new(0, -SIZE), PI / -2.0),
    ];

    for (position, angle) in lines.iter() {
        commands
            .spawn(ParticleSystemBundle {
                particle_system: ParticleSystem {
                    max_particles: 30_000,
                    texture: ParticleTexture::Sprite(asset_server.load("flame-particle.png")),
                    spawn_rate_per_second: ValueOverTime::Constant(1.0),
                    initial_speed: JitteredValue {
                        value: 10.0,
                        jitter_range: Some(-20.0..100.0),
                    },
                    initial_rotation: JitteredValue {
                        value: 0.0,
                        jitter_range: Some(-PI..PI),
                    },
                    acceleration: ValueOverTime::Sin(SinWave {
                        amplitude: 20.0,
                        period: 2.0,
                        phase_shift: 0.0,
                        vertical_shift: 0.0,
                    }),
                    lifetime: JitteredValue {
                        value: 1.0,
                        jitter_range: Some(-1.0..5.0),
                    },
                    color: ColorOverTime::Gradient(Gradient::new(vec![
                        ColorPoint::new(Color::hsla(10.0, 1.0, 0.3, 0.05), 0.0), // orange red
                        ColorPoint::new(Color::hsla(30.0, 1.0, 0.5, 0.05), 0.1), // yellow
                        ColorPoint::new(Color::hsla(30.0, 1.0, 0.7, 0.05), 0.2), // bright yellow
                        ColorPoint::new(Color::hsla(30.0, 1.0, 0.5, 0.05), 0.3), // yellow
                        ColorPoint::new(Color::hsla(10.0, 1.0, 0.1, 0.05), 0.6), // dark orange red
                        ColorPoint::new(Color::hsla(0.0, 0.0, 0.4, 0.01), 0.7),  // smoke gray
                        ColorPoint::new(Color::hsla(0.0, 0.0, 0.0, 0.00), 1.0),  // transparent
                    ])),
                    scale: ValueOverTime::Lerp(Lerp { a: 1.0, b: 6.0 }),
                    looping: true,
                    system_duration_seconds: 10.0,
                    max_distance: None,
                    z_value_override: Some(JitteredValue {
                        value: Layers::FLAME,
                        jitter_range: None,
                    }),
                    rotation_speed: JitteredValue {
                        value: 0.0,
                        jitter_range: Some(-10.0..10.0),
                    },
                    despawn_on_finish: true,
                    emitter_shape: EmitterShape::Line {
                        length: 100.0,
                        angle: JitteredValue {
                            value: *angle,
                            jitter_range: Some(-0.01..0.01),
                        },
                    },
                    ..default()
                },
                transform: Transform::from_translation(
                    Vec2::from(position).mul(GRID_SIZE).extend(Layers::FLAME),
                ),
                ..default()
            })
            .insert(Playing);
    }
}

fn update_shape(mut flames: Query<&mut ParticleSystem>, time: Res<Time>) {
    for mut flame in flames.iter_mut() {
        match &mut flame.emitter_shape {
            EmitterShape::CircleSegment { ref mut radius, .. } => {
                if radius.value > 2_000.0 {
                    continue;
                }
                radius.value += time.delta().as_secs_f32() * 200.0;
            }
            EmitterShape::Line {
                ref mut length,
                ..
            } => {
                if *length > 4_000.0 {
                    continue;
                }
                *length += time.delta().as_secs_f32() * 800.0;
                flame.spawn_rate_per_second = ValueOverTime::Constant(*length / 2.0);
            }
        }
    }
}

fn extinguish(
    mut commands: Commands,
    mut flames: Query<Entity, With<ParticleSystem>>,
    mut challenge_events: EventReader<ChallengeEvent>,
) {
    for event in challenge_events.iter() {
        if event == &ChallengeEvent::ChallengeComplete {
            for flame in flames.iter_mut() {
                commands.entity(flame).remove::<Playing>();
            }
        }
    }
}

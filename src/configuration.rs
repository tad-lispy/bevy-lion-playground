use bevy::prelude::*;
use std::time::Duration;
use wasm_bindgen::prelude::*;

#[derive(Resource, Debug, Clone)]
pub struct Configuration {
    pub touch_sensitivity: f32,
    pub scale: f32,
    pub speed: f32,
    pub debug: bool,
    pub show_fps: bool,
    pub text_upscale: f32,

    // Feature flags
    pub serpentine_locomotion: bool,
    pub flame: bool,
}

impl Default for Configuration {
    fn default() -> Self {
        Self {
            touch_sensitivity: 0.25,
            scale: 5.0,
            speed: 3.0,
            debug: false,
            show_fps: true,
            text_upscale: 2.0,

            // Feature flags
            serpentine_locomotion: false,
            flame: false,
        }
    }
}

impl Configuration {
    pub fn init() -> Self {
        let default = Self::default();

        // TODO: Abstract.
        Self {
            touch_sensitivity: get_env("touch_sensitivity")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.touch_sensitivity),
            scale: get_env("scale")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.scale),
            speed: get_env("speed")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.speed),
            debug: get_env("debug")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.debug),
            show_fps: get_env("show_fps")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.show_fps),
            text_upscale: get_env("text_upscale")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.text_upscale),

            // Feature flags
            serpentine_locomotion: get_env("serpentine_locomotion")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.serpentine_locomotion),

            flame: get_env("flame")
                .map(|string| string.parse().unwrap())
                .unwrap_or(default.flame),
        }
    }

    pub fn step_duration(&self) -> Duration {
        let pace = 1.0 / self.speed;
        Duration::from_secs_f32(pace)
    }
}

#[cfg(not(target_arch = "wasm32"))]
fn get_env(name: &str) -> Option<String> {
    std::env::var(name).ok()
}

#[cfg(target_arch = "wasm32")]
fn get_env(name: &str) -> Option<String> {
    get_query_param(name)
}

#[wasm_bindgen(module = "/src/query-params.js")]
extern "C" {
    fn get_query_param(name: &str) -> Option<String>;
}

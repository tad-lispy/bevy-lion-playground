use core::fmt;

use crate::direction::Direction;

/// A Protraction represents continuation of a direction, as in movement, shape etc.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Protraction {
    Straight,
    Left,
    Right,
}

impl Protraction {
    pub fn apply_to<T>(&self, subject: &T) -> T
    where
        T: Protractable,
    {
        subject.protract(self)
    }

    pub fn    mirror(&self) -> Self {
        match self {
            Protraction::Straight => Protraction::Straight,
            Protraction::Left => Protraction::Right,
            Protraction::Right => Protraction::Left,
        }
    }
}

impl fmt::Display for Protraction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Protraction::Straight => "straight",
                Protraction::Left => "left",
                Protraction::Right => "right",
            }
        )
    }
}

pub trait Protractable {
    fn protraction_of(&self, other: &Self) -> Option<Protraction>;
    fn protract(&self, protraction: &Protraction) -> Self;
}

impl Protractable for Direction {
    fn protraction_of(&self, other: &Self) -> Option<Protraction> {
        match (other, self) {
            (Direction::North, Direction::North) => Protraction::Straight.into(),
            (Direction::North, Direction::East) => Protraction::Right.into(),
            (Direction::North, Direction::South) => None,
            (Direction::North, Direction::West) => Protraction::Left.into(),
            (Direction::East, Direction::North) => Protraction::Left.into(),
            (Direction::East, Direction::East) => Protraction::Straight.into(),
            (Direction::East, Direction::South) => Protraction::Right.into(),
            (Direction::East, Direction::West) => None,
            (Direction::South, Direction::North) => None,
            (Direction::South, Direction::East) => Protraction::Left.into(),
            (Direction::South, Direction::South) => Protraction::Straight.into(),
            (Direction::South, Direction::West) => Protraction::Right.into(),
            (Direction::West, Direction::North) => Protraction::Right.into(),
            (Direction::West, Direction::East) => None,
            (Direction::West, Direction::South) => Protraction::Left.into(),
            (Direction::West, Direction::West) => Protraction::Straight.into(),
        }
    }

    fn protract(&self, protraction: &Protraction) -> Self {
        match protraction {
            Protraction::Straight => self.to_owned(),
            Protraction::Left => match self {
                Direction::North => Direction::West,
                Direction::East => Direction::North,
                Direction::South => Direction::East,
                Direction::West => Direction::South,
            },
            Protraction::Right => match self {
                Direction::North => Direction::East,
                Direction::East => Direction::South,
                Direction::South => Direction::West,
                Direction::West => Direction::North,
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn protractions_of_direction() {
        assert_eq!(
            Direction::South
                .protract(&Protraction::Left)
                .protract(&Protraction::Right),
            Direction::South,
            "Face south, turn left and right and you should face south again"
        );

        assert_eq!(
            Direction::West
                .protract(&Protraction::Right)
                .protract(&Protraction::Straight)
                .protract(&Protraction::Left)
                .protraction_of(&Direction::West),
            Some(Protraction::Straight),
            "Face south, turn left and right and you should face south again"
        );
    }
}

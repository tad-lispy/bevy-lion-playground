use crate::direction::{Direction, Shiftable};
use bevy::prelude::*;
use derive_more::From;
use rand::{rngs::ThreadRng, Rng};

/// Logically the game is played on a 2d grid, so a position is represented as
/// an integer vector.
#[derive(Component, Debug, From, Deref, DerefMut, Eq, PartialEq, Default, Hash, Clone)]
pub struct Position {
    coordinates: IVec2,
}

impl Position {
    pub fn new(x: i32, y: i32) -> Self {
        IVec2::new(x, y).into()
    }

    pub fn random(rng: &mut ThreadRng, center: Self, distance: u32) -> Self {
        let range = -(distance as i32)..(distance as i32);
        let displacement = IVec2::new(rng.gen_range(range.clone()), rng.gen_range(range));
        let coordinates = center.coordinates + displacement;
        debug!("Generated random position at {coordinates:?}, i.e {displacement:?} away from {center:?}");
        Self { coordinates }
    }
}

impl From<&Position> for Vec2 {
    fn from(value: &Position) -> Self {
        value.coordinates.as_vec2()
    }
}

/// A [position](Position) can be shifted in a given
/// [direction](crate::direction::Direction).
///
/// ```
/// assert_eq!(
///     Position::new(21,  -6).shift(&Direction::North).to_owned(),
///     Position::new(21,  -5)
/// );
/// ```
impl Shiftable for Position {
    fn shift(&mut self, direction: &Direction) -> &mut Self {
        self.coordinates.shift(direction);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_position() {
        let position: Position = Position::new(10, -5);
        assert_eq!(
            position.coordinates.x, 10,
            "The X coordinate of new position is wrong."
        );
        assert_eq!(
            position.coordinates.y, -5,
            "The Y coordinate of new position is wrong."
        );
    }

    #[test]
    fn shifting_north() {
        assert_eq!(
            Position::new(21, -6).shift(&Direction::North).to_owned(),
            Position::new(21, -5)
        )
    }

    #[test]
    fn shifting_east() {
        assert_eq!(
            Position::new(21, -6).shift(&Direction::East).to_owned(),
            Position::new(22, -6)
        );
    }

    #[test]
    fn shifting_south() {
        assert_eq!(
            Position::new(21, -6).shift(&Direction::South).to_owned(),
            Position::new(21, -7)
        );
    }

    #[test]
    fn shifting_west() {
        assert_eq!(
            Position::new(21, -6).shift(&Direction::West).to_owned(),
            Position::new(20, -6)
        );
    }

    #[test]
    fn chain_shifting() {
        assert_eq!(
            Position::new(21, -6)
                .shift(&Direction::West)
                .shift(&Direction::West)
                .shift(&Direction::South)
                .to_owned(),
            Position::new(19, -7)
        );
    }

    #[test]
    fn shift_is_a_mutation() {
        let mut position = Position::new(0, 0);
        position.shift(&Direction::North);

        assert_eq!(position, Position::new(0, 1));
    }
}

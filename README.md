A playground where I learn [Bevy][] and [Lyon][] for 2d geometric shapes for game graphics. A vague goal is to re-implement [Word Snake][] in Rust, but for now this is just a playground.

# Serpentine locomotion

The serpentine locomotion is the main mode of locomotion for snakes (or
at least most commonly associated with them). Here are some good videos
to get the general idea:

  - [How Snakes Move!](https://www.youtube.com/watch?v=7-AKPFiIEEw) by Snake Discovery

    Discussing different modes of locomotion.
    
  - [Snake Locomotion](https://youtu.be/_RerCt4GAO8?t=44) by National Science Fundation

    Contains a clip showing how curves of the middle section of the
    snake in serpentine locomotion is stationary.


## Representation in the Game

The idea behind this concept is to split each segment of the snake in 4 sections. Looking from the head towards the tail of a straight snake, each segment except the head and the tip of the tail is composed like this:

  1. The first section goes toward a given direction (North, East, South or West), away from the front and it's length is half of the grid size. It is shifted 1 / 2 grid size to the right, so for example if the direction is West, it's shifted to the North.

  2. The next section is a line at the straight angle left. It's length is also 1 / 2 grid size. So it has a symmetry in relation to the snake axis.

  3. The third section turns to the right and continues in the given direction for another 1 / 2 of the grid size.

  4. The last section goes 1 / 2 of the grid size to the left.

![Serpentine Locomotion diagram](./art/serpentine-locomotion-concept.svg)

All these middle segments will remain static as the snake moves. Only the head and tip of the tail are animated.


## Head Segment

The head has a bit of a different shape in that the first section is not shifted to the right. It is on the segment axis. What follows, is that second section only goes 1 / 4 of the GS left. The last two sections are the same as in middle segments.

> TODO: Describe how the head is animated.


## Tip of the Tail Segment

The tip of the tail is an inversion of the head. First two sections are the same as in middle segments. Third is shorter and only goes toward the axis. Last section lays on the axis.

> TODO: Describe how the tail is animated


## Pivotal segments

> TODO: Describe the goemetry of a pivotal segment, including the pivoting head and tail animations.


# Credits

[The Cartographer pattern](https://www.toptal.com/designers/subtlepatterns/cartographer/) used in the background made by [Sam Feyaerts](http://sam.feyaerts.me/)

[The Cantarell font](https://cantarell.gnome.org/) by [Dave Crossland](https://github.com/davelab6/) is licensed under [the SIL Open Font License, Version 1.1](https://scripts.sil.org/OFL).


[Bevy]: https://bevyengine.org/
[Lyon]: https://docs.rs/lyon/latest/lyon/
[Word Snake]: https://gitlab.com/software-garden/word-snake
